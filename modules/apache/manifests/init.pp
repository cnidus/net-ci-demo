class apache {

    $str = "deb http://repo.cumulusnetworks.com CumulusLinux-2.5 main addons updates\ndeb http://repo.cumulusnetworks.com CumulusLinux-2.5 security-updates\ndeb http://ftp.us.debian.org/debian/ wheezy main contrib non-free\ndeb-src http://ftp.us.debian.org/debian/ wheezy main contrib non-free"

    file { "apt-install-debian-sources":
      path => "/etc/apt/sources.list",
      content => $str,
    }

    exec { "apt-update":
      command => "/usr/bin/apt-get update",
    }

    package { 'apache2':
        ensure => 'installed',
        provider => 'apt',
    }

    service { 'apache2':
        ensure     => running,
        enable     => true,
        hasrestart => true,
        hasstatus  => true,
    }
}
